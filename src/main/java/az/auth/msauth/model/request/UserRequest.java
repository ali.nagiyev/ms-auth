package az.auth.msauth.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static lombok.AccessLevel.PRIVATE;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class UserRequest {

    @NotNull(message = "Username can not be empty")
    String userName;

    @NotBlank(message = "Password is required")
    @Size(min = 6, max = 20 ,message = "Password should have min 6 characters and max 20 characters")
    String password;
}
