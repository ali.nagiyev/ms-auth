package az.auth.msauth.controller;

import az.auth.msauth.model.request.UserRequest;
import az.auth.msauth.model.response.TokenResponse;
import az.auth.msauth.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static lombok.AccessLevel.PRIVATE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class UserController {

    UserService userService;

    @PostMapping("/registration")
    public void registrationUser(
           @Valid @RequestBody UserRequest userRequest
    ) {
        userService.registerUser(userRequest);
    }

    @PostMapping("/login")
    public TokenResponse login(
            @RequestBody UserRequest userRequest
    ) {

        return userService.login(userRequest);
    }
}
